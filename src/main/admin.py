from django.contrib import admin  # NOQA

from groups.models import Group
from students.models import Student
from teachers.models import Teacher

admin.site.register([Student, Group, Teacher])
