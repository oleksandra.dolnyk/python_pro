import os

from django.core.validators import MinLengthValidator
from django.db import models


class Person(models.Model):
    first_name = models.CharField(max_length=120, null=True, blank=True, validators=[MinLengthValidator(2)])
    last_name = models.CharField(max_length=120, null=True, blank=True)
    email = models.EmailField(max_length=150, null=True, blank=True)
    birth_date = models.DateField(null=True, blank=True)

    class Meta:
        abstract = True


def cv_upload_path(instance, filename):
    name = f"{instance.first_name}_{instance.last_name}".replace(" ", "_").lower()
    upload_dir = f"cv/{name}"
    return os.path.join(upload_dir, filename)


def avatar_upload_path(instance, filename):
    name = f"{instance.first_name}_{instance.last_name}".replace(" ", "_").lower()
    upload_dir = f"avatars/{name}"
    return os.path.join(upload_dir, filename)
