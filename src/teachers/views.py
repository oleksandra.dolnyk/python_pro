from django.db.models import Q
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from webargs import fields
from webargs.djangoparser import use_args

from teachers.forms import TeacherForm
from teachers.models import Teacher


@use_args(
    {
        "first_name": fields.Str(
            required=False,
        ),
        "last_name": fields.Str(
            required=False,
        ),
        "search": fields.Str(
            required=False,
        ),
    },
    location="query",
)
def get_teachers(request, params):
    teachers = Teacher.objects.all()

    search_fields = ["first_name", "last_name", "email"]

    for param_name, param_value in params.items():
        if param_name == "search":
            or_filter = Q()
            for field in search_fields:
                or_filter |= Q(**{f"{field}__icontains": param_value})
            teachers = teachers.filter(or_filter)
        else:
            teachers = teachers.filter(**{param_name: param_value})

    return render(request, template_name="teachers/teacher_list.html", context={"teachers": teachers})


def create_teacher(request):
    if request.method == "POST":
        form = TeacherForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("teachers:all_teachers"))
    elif request.method == "GET":
        form = TeacherForm()
    return render(request, template_name="teachers/teacher_create.html", context={"form": form})


def update_teacher(request, pk):
    teacher = get_object_or_404(Teacher.objects.all(), pk=pk)
    if request.method == "POST":
        form = TeacherForm(request.POST, request.FILES, instance=teacher)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("teachers:all_teachers"))
    elif request.method == "GET":
        form = TeacherForm(instance=teacher)
    return render(request, template_name="teachers/teacher_update.html", context={"form": form})


def delete_teacher(request, pk):
    teacher = get_object_or_404(Teacher.objects.all(), pk=pk)
    if request.method == "POST":
        teacher.delete()
        return HttpResponseRedirect(reverse("teachers:all_teachers"))
    elif request.method == "GET":
        pass
    return render(request, template_name="teachers/teacher_delete.html", context={"teacher": teacher})
