import random
from django.db import models
from faker import Faker

from groups.models import Group
from main.models import Person, avatar_upload_path

random = random.Random()


class Teacher(Person):
    international_certificates = models.BooleanField(default=False, null=True, blank=True)
    previous_work_place = models.CharField(max_length=120, null=True, blank=True)
    experience = models.BooleanField(default=False, null=True, blank=True)
    availability_per_week = models.PositiveSmallIntegerField(null=True, blank=True)
    groups = models.ManyToManyField(Group, related_name="teachers")
    avatar = models.ImageField(upload_to=avatar_upload_path, null=True, blank=True)

    @classmethod
    def generate_instances(cls, count):
        fake = Faker()
        for _ in range(count):
            teacher = cls.objects.create(
                first_name=fake.first_name(),
                last_name=fake.last_name(),
                email=fake.email(),
                birth_date=fake.date_of_birth(minimum_age=18, maximum_age=60),
                international_certificates=random.choice([True, False]),
                previous_work_place=fake.company(),
                experience=random.choice([True, False]),
                availability_per_week=random.randint(10, 40),
            )
            groups = Group.objects.filter(id__in=random.sample(range(1, 41), random.randint(1, 5)))
            teacher.groups.set(groups)

    def __str__(self):
        return f"{self.first_name} {self.last_name} {self.email} ({self.pk})"
