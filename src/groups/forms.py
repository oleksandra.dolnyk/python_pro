from django.forms import ModelForm

from groups.models import Group


class GroupForm(ModelForm):
    class Meta:
        model = Group
        fields = ["group_name", "start_date", "cost", "amount_of_students"]

    @staticmethod
    def normalize_text(text: str) -> str:
        return text.strip().capitalize()

    def clean_group_name(self):
        return self.normalize_text(self.cleaned_data["group_name"])
