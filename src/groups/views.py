from django.db.models import Q
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from webargs import fields
from webargs.djangoparser import use_args

from groups.forms import GroupForm
from groups.models import Group


@use_args(
    {
        "group_name": fields.Str(
            required=False,
        ),
        "start_date": fields.Str(
            required=False,
        ),
        "search": fields.Str(
            required=False,
        ),
    },
    location="query",
)
def get_groups(request, params):
    groups = Group.objects.all()

    search_fields = ["group_name", "start_date", "cost"]

    for param_name, param_value in params.items():
        if param_name == "search":
            or_filter = Q()
            for field in search_fields:
                or_filter |= Q(**{f"{field}__icontains": param_value})
            groups = groups.filter(or_filter)
        else:
            groups = groups.filter(**{param_name: param_value})

    return render(request, template_name="groups/group_list.html", context={"groups": groups})


def create_group(request):
    if request.method == "POST":
        form = GroupForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("groups:all_groups"))
    elif request.method == "GET":
        form = GroupForm(request.POST)

    return render(request, template_name="groups/group_create.html", context={"form": form})


def update_group(request, pk):
    group = get_object_or_404(Group.objects.all(), pk=pk)
    if request.method == "POST":
        form = GroupForm(request.POST, instance=group)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("groups:all_groups"))
    elif request.method == "GET":
        form = GroupForm(instance=group)

    return render(request, template_name="groups/group_update.html", context={"form": form})


def delete_group(request, pk):
    group = get_object_or_404(Group.objects.all(), pk=pk)
    if request.method == "POST":
        group.delete()
        return HttpResponseRedirect(reverse("groups:all_groups"))
    elif request.method == "GET":
        pass

    return render(request, template_name="groups/group_delete.html", context={"group": group})


def group_detail(request, pk):
    group = get_object_or_404(Group.objects.all(), pk=pk)
    return render(request, "groups/group_detail.html", {"group": group})
