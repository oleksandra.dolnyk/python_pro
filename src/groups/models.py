import random

from django.db import models
from faker import Faker


random = random.Random()


class Group(models.Model):
    group_name = models.CharField(max_length=120, blank=True)
    start_date = models.DateField(null=True, blank=True)
    cost = models.PositiveIntegerField(null=True, blank=True)
    amount_of_students = models.PositiveSmallIntegerField(null=True, blank=True)
    rating = models.PositiveSmallIntegerField(null=True, blank=True)
    description = models.CharField(max_length=300, null=True, blank=True)

    @classmethod
    def generate_instances(cls, count):
        fake = Faker()
        for _ in range(count):
            cls.objects.create(
                group_name=random.choice(
                    [
                        "Python Beginner",
                        "Python Pro",
                        "Java Beginner",
                        "Java Pro",
                        "JavaScript Beginner",
                        "JavaScript Pro",
                        "HR",
                        "Marketing",
                        "Python Testing",
                        "Java Testing",
                        "Design",
                    ]
                ),
                start_date=fake.date_this_year(before_today=False, after_today=True),
                cost=random.choice(["13999", "14999", "20999", "10999", "12999"]),
                amount_of_students=random.randint(10, 16),
                rating=random.randint(0, 5),
                description=fake.text(max_nb_chars=300),
            )

    def __str__(self):
        return (
            f"({self.pk}) {self.group_name} starts at {self.start_date}, costs {self.cost}₴. "
            f"Description: {self.description}"
        )
