from django.db.models import Q
from django.http import HttpResponseRedirect
from webargs import fields
from webargs.djangoparser import use_args

from students.forms import StudentForm
from students.models import Student
from django.shortcuts import get_object_or_404, render
from django.urls import reverse


def index(request):
    return render(request, template_name="index.html", context={"key": "Oleksandra"})


@use_args(
    {
        "first_name": fields.Str(
            required=False,
        ),
        "last_name": fields.Str(
            required=False,
        ),
        "search": fields.Str(
            required=False,
        ),
    },
    location="query",
)
def get_students(request, params):
    students = Student.objects.all()

    search_fields = ["first_name", "last_name", "email"]

    for param_name, param_value in params.items():
        if param_name == "search":
            or_filter = Q()
            for field in search_fields:
                or_filter |= Q(**{f"{field}__icontains": param_value})
            students = students.filter(or_filter)
        else:
            students = students.filter(**{param_name: param_value})

    return render(request, template_name="students/student_list.html", context={"students": students})


def create_student(request):
    if request.method == "POST":
        form = StudentForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("students:all_students"))
    elif request.method == "GET":
        form = StudentForm()

    return render(request, template_name="students/student_create.html", context={"form": form})


def update_student(request, pk):
    student = get_object_or_404(Student.objects.all(), pk=pk)
    if request.method == "POST":
        form = StudentForm(request.POST, request.FILES, instance=student)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("students:all_students"))
    elif request.method == "GET":
        form = StudentForm(instance=student)

    return render(request, template_name="students/student_update.html", context={"form": form})


def delete_student(request, pk):
    student = get_object_or_404(Student.objects.all(), pk=pk)
    if request.method == "POST":
        student.delete()
        return HttpResponseRedirect(reverse("students:all_students"))
    elif request.method == "GET":
        pass

    return render(request, template_name="students/student_delete.html", context={"student": student})
