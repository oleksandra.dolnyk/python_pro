import datetime
import random

from django.db import models
from faker import Faker

from groups.models import Group
from main.models import Person, cv_upload_path, avatar_upload_path


class Student(Person):
    grade = models.PositiveSmallIntegerField(null=True, blank=True)
    group = models.ForeignKey(Group, on_delete=models.CASCADE, related_name="students")
    avatar = models.ImageField(upload_to=avatar_upload_path, null=True, blank=True)
    cv = models.FileField(upload_to=cv_upload_path, null=True, blank=True)

    @classmethod
    def generate_instances(cls, count):
        fake = Faker()
        for _ in range(count):
            cls.objects.create(
                first_name=fake.first_name(),
                last_name=fake.last_name(),
                email=fake.email(),
                birth_date=fake.date_of_birth(minimum_age=18, maximum_age=60),
                group=Group.objects.get(id=random.randint(1, 40)),
            )

    def age(self):
        return datetime.datetime.now().year - self.birth_date.year

    def __str__(self):
        return f"{self.first_name} {self.last_name} {self.email} ({self.pk})"
